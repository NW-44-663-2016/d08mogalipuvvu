﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace D08Mogalipuvvu.Models
{
    public static class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            if (!context.Locations.Any())
            {
                // DB already seeded


                context.Locations.AddRange(
                     new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" },
                new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" },
                new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" },
                new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" },
                new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" },
                new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" }
                 );

                context.producers.AddRange(
                 new Producer() { LastName = "Michael", FirstName = "Mann", DateOfBirth = new DateTime(1923, 1, 23), Language = "English", LocationId = 1 },
                 new Producer() { LastName = "Dil", FirstName = "Raju", DateOfBirth = new DateTime(1931, 3, 03), Language = "Telugu", LocationId = 2 },
                 new Producer() { LastName = "Daggubati", FirstName = "Ramanaidu", DateOfBirth = new DateTime(1967, 4, 23), Language = "Telugu", LocationId = 3 },
                 new Producer() { LastName = "Gupta", FirstName = "Sanjay", DateOfBirth = new DateTime(1989, 5, 23), Language = "Hindi", LocationId = 4 },
                 new Producer() { LastName = "Bhatt", FirstName = "Mahesh", DateOfBirth = new DateTime(1990, 2, 13), Language = "Hindi", LocationId = 5 },
                 new Producer() { LastName = " Akhtar", FirstName = "Farhan", DateOfBirth = new DateTime(1981, 4, 17), Language = "Hindi", LocationId = 6 }
                 );

                context.SaveChanges();
            }
        }
    }
}
