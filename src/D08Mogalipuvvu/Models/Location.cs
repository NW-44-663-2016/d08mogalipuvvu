﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace D08Mogalipuvvu.Models
{
    public class Location
    {
        [Required]
        [Display(Name = "LocationId")]
        public int LocationID { get; set; }

        [Display(Name = "country name")]
        public string Country { get; set; }

        [Display(Name = "Place Name")]
        public string Place { get; set; }

        [Display(Name = "State Name")]
        public string State { get; set; }

        public string StateAbbreviation { get; set; }

        public string County { get; set; }

        public string ZipCode { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
