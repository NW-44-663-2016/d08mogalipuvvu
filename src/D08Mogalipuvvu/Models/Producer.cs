﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace D08Mogalipuvvu.Models
{
    public class Producer
    {
        [Required]
        [Display(Name = "ProducerId")]
        public int ProducerID { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Date Of Birth")]
        public DateTime DateOfBirth { get; set; }

        public string Language { get; set; }

        public int LocationId { get; set; }
    }
}
