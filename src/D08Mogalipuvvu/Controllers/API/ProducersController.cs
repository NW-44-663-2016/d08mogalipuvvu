using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Mogalipuvvu.Models;

namespace D08Mogalipuvvu.Controllers
{
    [Produces("application/json")]
    [Route("api/Producers")]
    public class ProducersController : Controller
    {
        private AppDbContext _context;

        public ProducersController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Producers
        [HttpGet]
        public IEnumerable<Producer> Getproducers()
        {
            return _context.producers;
        }

        // GET: api/Producers/5
        [HttpGet("{id}", Name = "GetProducer")]
        public IActionResult GetProducer([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Producer producer = _context.producers.Single(m => m.ProducerID == id);

            if (producer == null)
            {
                return HttpNotFound();
            }

            return Ok(producer);
        }

        // PUT: api/Producers/5
        [HttpPut("{id}")]
        public IActionResult PutProducer(int id, [FromBody] Producer producer)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != producer.ProducerID)
            {
                return HttpBadRequest();
            }

            _context.Entry(producer).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProducerExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Producers
        [HttpPost]
        public IActionResult PostProducer([FromBody] Producer producer)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.producers.Add(producer);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ProducerExists(producer.ProducerID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetProducer", new { id = producer.ProducerID }, producer);
        }

        // DELETE: api/Producers/5
        [HttpDelete("{id}")]
        public IActionResult DeleteProducer(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Producer producer = _context.producers.Single(m => m.ProducerID == id);
            if (producer == null)
            {
                return HttpNotFound();
            }

            _context.producers.Remove(producer);
            _context.SaveChanges();

            return Ok(producer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProducerExists(int id)
        {
            return _context.producers.Count(e => e.ProducerID == id) > 0;
        }
    }
}